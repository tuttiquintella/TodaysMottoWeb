class Phrase < ActiveRecord::Base
	@last_phrase_index = -1

	def self.find_diff_phrase
		new_phrase_index = 0
		loop do
			new_phrase_index = Phrase.find(1 + rand(Phrase.all.count))
			break if (new_phrase_index.id != @last_phrase_index)
		end

		@last_phrase_index = new_phrase_index.id
		new_phrase_index
	end
end
